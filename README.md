# randomfox

This is an library to get an link to an random fox image

## Installation ✅
```
npm i randomfox

or

yarn install randomfox

or

npm i git+https://gitlab.com/Minecodes13/randomfox.git

or

yarn install git+https://gitlab.com/Minecodes13/randomfox.git

or

npm i git+https://github.com/Minecodes/randomfox.git

or

yarn install git+https://github.com/Minecodes/randomfox.git
```
## Docs 🧐🤓

Example

```
const randomfox = require("randomfox");

console.log(fox());
```

Random fox Image

```
const { fox } = require("randomfox");

console.log(fox());
```

Custom fox Images

```
const { customfox } = require("../src/main");

console.log(customfox([
	"https://randomfox.ca/images/1.jpg",
	"https://randomfox.ca/images/2.jpg",
	"https://randomfox.ca/images/3.jpg"]));
```
